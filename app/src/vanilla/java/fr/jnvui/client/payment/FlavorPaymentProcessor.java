package fr.jnvui.client.payment;

import fr.jnvui.client.models.Payment;
import fr.jnvui.client.activities.TrackedActivity;

public abstract class FlavorPaymentProcessor extends PaymentProcessor {

    protected FlavorPaymentProcessor(TrackedActivity parentActivity, PaymentListener listener, Payment payment) {
        super(parentActivity, listener, payment);
    }

}
