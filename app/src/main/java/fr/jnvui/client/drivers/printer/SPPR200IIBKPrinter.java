package fr.jnvui.client.drivers.printer;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.widget.Toast;

import fr.jnvui.client.Configure;
import fr.jnvui.client.R;
import jpos.JposException;
import jpos.POSPrinter;
import jpos.POSPrinterConst;
import jpos.config.JposEntry;

import com.bxl.config.editor.BXLConfigLoader;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;

import fr.jnvui.client.utils.exception.CouldNotConnectException;
import fr.jnvui.client.utils.exception.CouldNotDisconnectException;

/**
 * Created by jips on 10/29/16.
 */

public class SPPR200IIBKPrinter extends BasePrinter {

    private BXLConfigLoader bxlConfigLoader;
    private POSPrinter posPrinter;
    private int brightness = 50;
    //	private int compress = 1;
    private int compress = 0;

    protected SPPR200IIBKPrinter(Handler handler) {
        super(handler);

        bxlConfigLoader = new BXLConfigLoader(ctx);
        try {
            bxlConfigLoader.openFile();
        } catch (Exception e) {
            e.printStackTrace();
            bxlConfigLoader.newFile();
        }
        posPrinter = new jpos.POSPrinter(ctx);
    }

    public SPPR200IIBKPrinter(Handler handler, String address) {
        super(handler, address);

        bxlConfigLoader = new BXLConfigLoader(ctx);
        try {
            bxlConfigLoader.openFile();
        } catch (Exception e) {
            e.printStackTrace();
            bxlConfigLoader.newFile();
        }
        posPrinter = new jpos.POSPrinter(ctx);
        bxlConfigLoader.removeEntry("SPP-R200II");
        bxlConfigLoader.addEntry("SPP-R200II",
                BXLConfigLoader.DEVICE_CATEGORY_POS_PRINTER, "SPP-R200II",
                BXLConfigLoader.DEVICE_BUS_BLUETOOTH, "74:F0:7D:E4:70:BE");

        bxlConfigLoader.saveFile();
    }

    @Override
    public boolean isConnected() {
        try {
            return posPrinter.getDeviceEnabled();
        } catch (JposException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void connect() throws CouldNotConnectException {
        this.printConnectTries = 0;
        this.maxConnectTries = Configure.getPrinterConnectTry();

        try {
            posPrinter.open("SPP-R200II");
            posPrinter.claim(0);
            posPrinter.setDeviceEnabled(true);
        } catch (JposException e) {
            e.printStackTrace();
            try {
                posPrinter.close();
            } catch (JposException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }
            throw new CouldNotConnectException();
        }

    }

    @Override
    public void disconnect() throws CouldNotDisconnectException {
        try {
            posPrinter.close();
        } catch (JposException e) {
            e.printStackTrace();
            throw new CouldNotDisconnectException();
        }

    }

    @Override
    protected void printLine(String data) {
        try {
            posPrinter.printNormal(POSPrinterConst.PTR_S_RECEIPT, data);

        } catch (JposException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void printLine() {
        try {
            posPrinter.printNormal(POSPrinterConst.PTR_S_RECEIPT, "\n");
        } catch (JposException e) {
            e.printStackTrace();
        }

    }

    @Override
    protected void cut() {

    }

    @Override
    protected void printBitmap(Bitmap bitmap) {
        ByteBuffer buffer = ByteBuffer.allocate(4);
        buffer.put((byte) POSPrinterConst.PTR_S_RECEIPT);
        buffer.put((byte) brightness);
        buffer.put((byte) compress);
        buffer.put((byte) 0x00);

        try {
            posPrinter.printBitmap(buffer.getInt(0), bitmap,
                    150, POSPrinterConst.PTR_BM_CENTER);
        } catch (JposException e) {
            e.printStackTrace();
        }
    }



    @Override
    protected void printLogo() {
        InputStream is = null;
        is = ctx.getResources().openRawResource(R.raw.logo);
        Bitmap bitmap = BitmapFactory.decodeStream(is);

        printBitmap(bitmap);
    }

    @Override
    protected void printHeader() {
        printLine(ctx.getString(R.string.header));
    }

    @Override
    protected void printFooter() {
        printLine(ctx.getString(R.string.footer));

    }


}
