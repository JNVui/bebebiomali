/*
    Pasteque Android client
    Copyright (C) Pasteque contributors, see the COPYRIGHT file

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package fr.jnvui.client.drivers.printer;

import fr.jnvui.client.models.CashRegister;
import fr.jnvui.client.models.Receipt;
import fr.jnvui.client.models.ZTicket;
import fr.jnvui.client.utils.exception.CouldNotConnectException;
import fr.jnvui.client.utils.exception.CouldNotDisconnectException;

public interface Printer {

    void connect() throws CouldNotConnectException;
    void disconnect() throws CouldNotDisconnectException;
    void printReceipt(Receipt r);
    void printZTicket(ZTicket z, CashRegister cr);
    void printTest();
}
